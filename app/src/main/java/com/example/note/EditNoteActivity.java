package com.example.note;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class EditNoteActivity extends AppCompatActivity {
    NoteDatabase noteDatabase;
    EditText etTitle;
    EditText etContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        setTitle("Sửa");
        etTitle = findViewById(R.id.etTitle);
        etContent = findViewById(R.id.etContent);

        noteDatabase = new NoteDatabase(getApplicationContext());
        Intent getIntent = getIntent();
        Note n = noteDatabase.getById(getIntent.getIntExtra("id", 0));
        Log.d("dl: ", n.getTitle());
        etTitle.setText(n.getTitle());
        etContent.setText(n.getContent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        Intent i = getIntent();
        switch (id) {
            case R.id.btnSave:
                if (etTitle.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Title is empty", Toast.LENGTH_SHORT).show();
                } else {
                    noteDatabase.update(i.getIntExtra("id", 0), new Note(etTitle.getText().toString(), etContent.getText().toString()));
                    startActivity(intent);
                }
                break;

            case R.id.btnDel:
                noteDatabase.destroy(i.getIntExtra("id", 0));
                Toast.makeText(getApplicationContext(), "Xóa id = " + i.getIntExtra("id", 0), Toast.LENGTH_SHORT).show();
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
