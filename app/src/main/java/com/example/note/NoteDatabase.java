package com.example.note;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class NoteDatabase extends SQLiteOpenHelper {
    public static final String DB_NAME = "note.db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "notes";
    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_CONTENT = "content";

    public NoteDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    SQLiteDatabase db;

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + "(" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL" + "," +
                KEY_TITLE + " TEXT NOT NULL" + "," +
                KEY_CONTENT + " TEXT NOT NULL" +
                ")";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<Note> getAll() {

        db = getWritableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(sql, null);

        List<Note> notes = new ArrayList<>();
        if (cursor != null) {
            cursor.moveToFirst();
            Note note;
            while (cursor.moveToNext()) {

                note = new Note();
                note.setId(cursor.getInt(0));
                note.setTitle(cursor.getString(1));
                note.setContent(cursor.getString(2));
                Log.d("data: ", String.valueOf(note.getId()));
                notes.add(note);

            }
        } else {
            Log.d("data: ", "null");
        }
        db.close();
        return notes;
    }
    public Note getById(int id) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = " + id;

        Note note = new Note();
        db = getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor != null ) {
            cursor.moveToFirst();
            note = cursorToNote(cursor);
        }
        return note;
    }

    public Note cursorToNote(Cursor cursor) {
        Note note = new Note();
        note.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
        note.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
        note.setContent(cursor.getString(cursor.getColumnIndex(KEY_CONTENT)));
        return note;
    }

    public long insert(Note note) {
        db = getWritableDatabase();
        long index = db.insert(TABLE_NAME, null, noteToValues(note));
        db.close();
        return index;
    }

    public long update(int id, Note note) {
        db = getWritableDatabase();
        long index = db.update(TABLE_NAME, noteToValues(note), KEY_ID +" = "+id, null);
        db.close();
        return index;
    }

    public long destroy(int id) {
        db = getWritableDatabase();
        long index = db.delete(TABLE_NAME, KEY_ID + " = " + id, null);
        db.close();
        return index;
    }

    public ContentValues noteToValues(Note note) {
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, note.getTitle());
        values.put(KEY_CONTENT, note.getContent());
        return values;
    }
}
