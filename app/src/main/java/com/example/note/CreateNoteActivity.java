package com.example.note;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class CreateNoteActivity extends AppCompatActivity {
    EditText etTitle;
    EditText etContent;

    public static  final String KEY_TITLE = "title";
    public static  final String KEY_CONTENT = "content";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);
        setTitle("Thêm");

        etTitle = findViewById(R.id.etTitle);
        etContent = findViewById(R.id.etContent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.btnSave:
                Log.d("hehe: ", etTitle.getText() + " " +etContent.getText());
                if (etTitle.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Title is empty", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    NoteDatabase noteDatabase = new NoteDatabase(getApplicationContext());
                    noteDatabase.insert(new Note(etTitle.getText().toString(), etContent.getText().toString()));

                    startActivity(intent);
                }
        }
        return super.onOptionsItemSelected(item);
    }
}
