package com.example.note;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView lvNote;
    List<Note> notes = new ArrayList<>();
    NoteAdapter noteAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvNote = findViewById(R.id.lvNote);

        NoteDatabase noteDatabase = new NoteDatabase(getApplicationContext());

        notes = noteDatabase.getAll();


        noteAdapter = new NoteAdapter(getApplicationContext(), notes);

        lvNote.setAdapter(noteAdapter);

        /*noteDatabase.insert(new Note("hehe", "123"));
        noteDatabase.insert(new Note("hehe2", "1234"));*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.btnAdd:
                startActivity(new Intent(getApplicationContext(), CreateNoteActivity.class));

        }
        return super.onOptionsItemSelected(item);
    }
}
